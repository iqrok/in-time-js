#ifndef _DELAY_WORKER_HPP_
#define _DELAY_WORKER_HPP_

#include <cstdint>
#include <napi.h>

class DelayWorker : public Napi::AsyncWorker {

public:
	DelayWorker(Napi::Function& callback, uint64_t& delay_ns, uint64_t& busy_ns,
	Napi::Promise::Deferred& deferred);
	virtual ~DelayWorker() {};

	void Execute() override;
	void OnOK() override;

private:
	uint64_t delay_ns;
	uint64_t busy_ns;
	int64_t pause_time_ns;
	Napi::Promise::Deferred deferred;
};

#endif
