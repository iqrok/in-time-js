#ifndef _NAPI_INTIME_H_
#define _NAPI_INTIME_H_

#include <map>
#include <thread>
#include <cstdint>

#include <napi.h>

#include "TimeoutWorker.h"
#include "DelayWorker.h"

struct SetIntervalCtx {
	SetIntervalCtx(Napi::Env env)
		: deferred(Napi::Promise::Deferred::New(env)) {};

	bool is_stopped;
	uint8_t counter = 0;
	uint32_t id;
	uint64_t period_ns;
	uint64_t busy_ns;
	Napi::Promise::Deferred deferred;
	std::thread thread;
	Napi::ThreadSafeFunction tsfn;
};

#endif
