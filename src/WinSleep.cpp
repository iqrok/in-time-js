#include "WinSleep.h"
#include <cstdio>
// https://gist.github.com/ngryman/6482577?permalink_comment_id=2637845#gistcomment-2637845
inline void WinSleep::usleep(const uint32_t& us)
{
	HANDLE timer;
	LARGE_INTEGER ft;

	// win32 timer resolution is 100 ns
	// https://learn.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-setwaitabletimer
	ft.QuadPart = -(10 * static_cast<int64_t>(us));

	timer = CreateWaitableTimer(NULL, TRUE, NULL);
	SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
	// start sleep
	WaitForSingleObject(timer, INFINITE);

	CloseHandle(timer);
}
