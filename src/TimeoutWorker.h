#ifndef _TIMEOUT_WORKER_HPP_
#define _TIMEOUT_WORKER_HPP_

#include <cstdint>
#include <map>

#include <napi.h>

class TimeoutWorker : public Napi::AsyncWorker {

public:
	TimeoutWorker(Napi::Function& callback, uint64_t delay_ns, uint64_t busy_ns,
		uint32_t id, std::map<uint32_t, TimeoutWorker*>* p_queue);
	virtual ~TimeoutWorker() {};

	void Execute() override;
	void OnOK() override;
	void Cancel();

private:
	uint64_t delay_ns;
	uint64_t busy_ns;
	int64_t pause_time_ns;
	uint32_t id;
	bool cancelled = false;
	std::map<uint32_t, TimeoutWorker*>* p_queue;
};

#endif
