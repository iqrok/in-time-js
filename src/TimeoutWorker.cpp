#include "TimeoutWorker.h"
#include "InTime.h"

#ifdef __linux__
#include "TimespecHelper.h"

#else
#include "Timestamp.hpp"

#if _WIN32
#include "WinSleep.h"
#endif

#endif

#ifndef NSEC_PER_SEC
#define NSEC_PER_SEC 1'000'000'000
#endif

#include <cstdio>

TimeoutWorker::TimeoutWorker(Napi::Function& callback, uint64_t delay_ns,
	uint64_t busy_ns, uint32_t id, std::map<uint32_t, TimeoutWorker*>* ptr)
	: Napi::AsyncWorker(callback)
	, delay_ns(delay_ns)
	, busy_ns(busy_ns)
	, id(id)
	, p_queue(ptr) {}

void TimeoutWorker::Execute()
{
#ifdef __linux__
	struct timespec start, finish, wakeup, current;
	uint64_t sleep_ns = delay_ns - busy_ns;

	Timespec::now(&start);
	Timespec::copy(&wakeup, start, sleep_ns);

	clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &wakeup, NULL);

	// busy wait
	if(busy_ns > 0){
		Timespec::now(&wakeup);
		wakeup.tv_nsec += busy_ns;

		while (wakeup.tv_nsec >= NSEC_PER_SEC) {
			wakeup.tv_nsec -= NSEC_PER_SEC;
			wakeup.tv_sec++;
		}

		Timespec::now(&current);
		while(Timespec::compare(wakeup, current)){
			Timespec::now(&current);
			clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &current, NULL);
		}
	}

	Timespec::now(&finish);
	Timespec::diff(finish, start, &pause_time_ns);

#elif _WIN32
	uint32_t pause_us = static_cast<uint32_t>((delay_ns - busy_ns) / 1000);
	uint64_t start = Timestamp::now_ns();

	WinSleep::usleep(pause_us);

	// busy wait
	if(busy_ns > 0){
		uint64_t wakeup = Timestamp::now_ns() + busy_ns;
		while(wakeup > Timestamp::now_ns()){
			WinSleep::usleep(0);
		}
	}

	pause_time_ns = Timestamp::now_ns() - start;

#else
	auto pause = std::chrono::microseconds(delay_ns);
	uint64_t start = Timestamp::now_ns();
	auto wakeup = std::chrono::steady_clock::now() + pause;

	std::this_thread::sleep_until(wakeup);

	pause_time_ns = Timestamp::now_ns() - start;

#endif
}

void TimeoutWorker::OnOK()
{
	if(!cancelled){
		Napi::Value jsval_pause_time
			= Napi::Number::New(Env(), static_cast<float>(pause_time_ns) / 1e3);

		Callback().Call({ jsval_pause_time });
	}

	p_queue->erase(id);
}

void TimeoutWorker::Cancel()
{
	cancelled = true;
}
