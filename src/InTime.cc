#include "InTime.h"

#ifdef __linux__
#include "TimespecHelper.h"

#else
#include "Timestamp.hpp"

#if _WIN32
#include "WinSleep.h"
#endif

#endif

#ifndef NSEC_PER_SEC
#define NSEC_PER_SEC 1'000'000'000
#endif

static uint32_t ids = 0;
static std::map<uint32_t, SetIntervalCtx*> queue_si;
static std::map<uint32_t, TimeoutWorker*> queue_st;
constexpr uint8_t max_count = 5;

/************************** Set Interval ******************************/

void finalizer_si(Napi::Env env, void* finalizeData, SetIntervalCtx* context)
{
	uint32_t qid = context->id;
	context->thread.join();
	context->deferred.Resolve(Napi::Boolean::New(env, true));

	delete context;
	queue_si.erase(qid);
}

void thread_si(SetIntervalCtx* context)
{
	auto routine_cb
		= [](Napi::Env env, Napi::Function js_cb, int64_t* elapsed) {
				Napi::Number elapsed_us
				  = Napi::Number::New(env, static_cast<float>(*elapsed) / 1e3);

				try {
					js_cb.Call({ elapsed_us });
				} catch(const Napi::Error& err) {
					Napi::Error::New(env, err.what())
						.ThrowAsJavaScriptException();
				}
			};

	uint64_t sleep_ns = context->period_ns - context->busy_ns;
	int64_t elapsed;

#ifdef __linux__
	struct timespec wakeup, start, stop;

#elif _WIN32
	uint64_t start;
	uint32_t pause_us = static_cast<uint32_t>(sleep_ns / 1000);

#else
	uint64_t start;
	auto pause = std::chrono::nanoseconds(sleep_ns);
	auto busy = std::chrono::nanoseconds(context->busy_ns);

#endif

	while (!context->is_stopped) {
#ifdef __linux__
		Timespec::now(&start);
		Timespec::copy(&wakeup, start, sleep_ns);

		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &wakeup, NULL);

		// busy wait
		if(context->busy_ns > 0){
			struct timespec current;
			context->counter = 0;

			Timespec::copy(&wakeup, start, context->period_ns);
			Timespec::now(&current);

			while(Timespec::compare(current, wakeup)){
				Timespec::now(&current);

				if(context->counter++ < max_count) continue;

				clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &current, NULL);
				context->counter = 0;
			}
		}

		Timespec::now(&stop);
		Timespec::diff(stop, start, &elapsed);

#elif _WIN32
		start = Timestamp::now_ns();

		WinSleep::usleep(pause_us);

		// busy wait
		if(context->busy_ns > 0){
			context->counter = 0;
			uint64_t wakeup = start + context->period_ns;
			while(wakeup > Timestamp::now_ns()){
				if(context->counter++ < max_count) continue;

				WinSleep::usleep(0);
				context->counter = 0;
			}
		}

		elapsed = Timestamp::now_ns() - start;

#else
		start = Timestamp::now_ns();
		auto wakeup = std::chrono::steady_clock::now() + pause;

		std::this_thread::sleep_until(wakeup);

		// busy wait
		if(context->busy_ns > 0){
			wakeup = std::chrono::steady_clock::now() + busy;
			while(wakeup > std::chrono::steady_clock::now()){ }
		}

		elapsed = Timestamp::now_ns() - start;

#endif
		if (context->tsfn.NonBlockingCall(&elapsed, routine_cb) != napi_ok) {
			Napi::Error::Fatal("thread_si", "NonBlockingCall() failed");
		}
	}

	context->tsfn.Abort();
}

Napi::Value js_set_interval(const Napi::CallbackInfo& info)
{
	Napi::Env env = info.Env();

	uint32_t qid = ids++;
	Napi::Function js_cb = info[0].As<Napi::Function>();
	uint64_t delay_ns
		= static_cast<uint64_t>(info[1].As<Napi::Number>().DoubleValue() * 1e3);
	uint64_t busy_ns = 0;

	if(info.Length() > 2){
		busy_ns	= static_cast<uint64_t>
			(info[2].As<Napi::Number>().DoubleValue() * 1e3);
	}

	queue_si[qid] = new SetIntervalCtx(env);

	queue_si[qid]->is_stopped = false;
	queue_si[qid]->id = qid;
	queue_si[qid]->period_ns = delay_ns;
	queue_si[qid]->busy_ns = busy_ns;

	// disable busy wait, if value is greater than the period
	if(queue_si[qid]->busy_ns > queue_si[qid]->period_ns){
		queue_si[qid]->busy_ns = 0;
	}

	queue_si.at(qid)->tsfn = Napi::ThreadSafeFunction::New(env, // Environment
		js_cb, // JS function from caller
		std::string("SetInterval") + std::to_string(qid), // Resource name
		0, // Max queue_si size (0 = unlimited).
		1, // Initial thread count
		queue_si.at(qid), // Context,
		finalizer_si, // Finalizer
		(void*)nullptr // Finalizer data
	);

	queue_si.at(qid)->thread = std::thread(thread_si, queue_si.at(qid));

	return Napi::Number::New(env, qid);
}

Napi::Value js_clear_interval(const Napi::CallbackInfo& info)
{
	Napi::Env env = info.Env();

	auto deferred = Napi::Promise::Deferred::New(env);
	uint32_t qid = info[0].As<Napi::Number>().Uint32Value();

	try {
		queue_si.at(qid)->is_stopped = true;
		deferred.Resolve(env.Undefined());
	} catch (const std::out_of_range& err) {
		deferred.Reject(Napi::TypeError::New(env, err.what()).Value());
	}

	return deferred.Promise();
}

/***************************** Set Timeout ************************************/

Napi::Value js_set_timeout(const Napi::CallbackInfo& info)
{
	Napi::Env env = info.Env();

	if (info.Length() < 2) {
		Napi::Error::New(env, "Needs 2 Parameters: Callback, nanoseconds")
			.ThrowAsJavaScriptException();
		return env.Null();
	}

	uint32_t qid = ids++;

	Napi::Function js_cb = info[0].As<Napi::Function>();
	uint64_t delay_ns
		= static_cast<uint64_t>(info[1].As<Napi::Number>().DoubleValue() * 1E3);
	uint64_t busy_ns = 0;

	if(info.Length() > 2){
		busy_ns	= static_cast<uint64_t>
			(info[2].As<Napi::Number>().DoubleValue() * 1e3);

		// disable busy wait, if value is greater than the delay
		if(busy_ns > delay_ns) busy_ns = 0;
	}

	queue_st[qid] = new TimeoutWorker(js_cb, delay_ns, busy_ns, qid, &queue_st);
	queue_st[qid]->Queue();

	return Napi::Number::New(env, qid);
}

Napi::Value js_cancel_timeout(const Napi::CallbackInfo& info)
{
	Napi::Env env = info.Env();

	auto deferred = Napi::Promise::Deferred::New(env);
	uint32_t qid = info[0].As<Napi::Number>().Uint32Value();

	try {
		queue_st.at(qid)->Cancel();
		deferred.Resolve(env.Undefined());
	} catch (const std::out_of_range& err) {
		deferred.Reject(Napi::TypeError::New(env, err.what()).Value());
	}

	return deferred.Promise();
}

/******************************** Delay ***************************************/

inline void wait_ns(const uint64_t& delay_ns, const uint64_t& busy_ns)
{
#if __linux__
	struct timespec start, wakeup, current;
	uint64_t sleep_ns = delay_ns - busy_ns;

	Timespec::now(&start);
	Timespec::copy(&wakeup, start, sleep_ns);

	clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &wakeup, NULL);

	// busy wait
	if(busy_ns > 0){
		Timespec::copy(&wakeup, start, delay_ns);

		Timespec::now(&current);
		while(Timespec::compare(wakeup, current)){
			Timespec::now(&current);
			clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &current, NULL);
		}
	}

#elif _WIN32
	uint32_t pause_us = static_cast<uint32_t>((delay_ns - busy_ns) / 1000);

	WinSleep::usleep(pause_us);

	// busy wait
	if(busy_ns > 0){
		uint64_t wakeup = Timestamp::now_ns() + busy_ns;
		while(wakeup > Timestamp::now_ns()){
			WinSleep::usleep(0);
		}
	}

#endif
}

Napi::Promise js_delay_ms(const Napi::CallbackInfo& info)
{
	Napi::Env env = info.Env();
	auto deferred = Napi::Promise::Deferred::New(env);

	if (info.Length() < 1) {
		deferred.Reject(Napi::Error::New(env, "Needs 1 Parameter: milliseconds")
			.Value());
		return deferred.Promise();
	}

	uint64_t delay_ns
		= static_cast<uint64_t>(info[0].As<Napi::Number>().DoubleValue() * 1e6);
	uint64_t busy_ns = 0;

	if (info.Length() > 1) {
		busy_ns = static_cast<uint64_t>
			(info[1].As<Napi::Number>().DoubleValue() * 1e6);
	}

	// TODO: should be an empty function, as it'll never be called
	Napi::Function cb = Napi::Function::New(env, js_delay_ms);

	DelayWorker* d_worker = new DelayWorker(cb, delay_ns, busy_ns, deferred);
	d_worker->Queue();

	return deferred.Promise();
}

Napi::Promise js_delay_us(const Napi::CallbackInfo& info)
{
	Napi::Env env = info.Env();
	auto deferred = Napi::Promise::Deferred::New(env);

	if (info.Length() < 1) {
		deferred.Reject(Napi::Error::New(env, "Needs 1 Parameter: micorseconds")
			.Value());
		return deferred.Promise();
	}

	uint64_t delay_ns
		= static_cast<uint64_t>(info[0].As<Napi::Number>().DoubleValue() * 1e3);
	uint64_t busy_ns = 0;

	if (info.Length() > 1) {
		busy_ns = static_cast<uint64_t>
			(info[1].As<Napi::Number>().DoubleValue() * 1e3);
	}

	// TODO: should be an empty function, as it'll never be called
	Napi::Function cb = Napi::Function::New(env, js_delay_us);

	DelayWorker* d_worker = new DelayWorker(cb, delay_ns, busy_ns, deferred);
	d_worker->Queue();

	return deferred.Promise();
}

Napi::Object init_napi(Napi::Env env, Napi::Object exports)
{
	exports.Set(Napi::String::New(env, "SetInterval"),
		Napi::Function::New(env, js_set_interval));
	exports.Set(Napi::String::New(env, "ClearInterval"),
		Napi::Function::New(env, js_clear_interval));
	exports.Set(Napi::String::New(env, "SetTimeout"),
		Napi::Function::New(env, js_set_timeout));
	exports.Set(Napi::String::New(env, "CancelTimeout"),
		Napi::Function::New(env, js_cancel_timeout));
	exports.Set(Napi::String::New(env, "DelayMs"),
		Napi::Function::New(env, js_delay_ms));
	exports.Set(Napi::String::New(env, "DelayUs"),
		Napi::Function::New(env, js_delay_us));

	return exports;
}

NODE_API_MODULE(NODE_GYP_MODULE_NAME, init_napi);
