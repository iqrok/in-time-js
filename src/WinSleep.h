#ifndef _WIN_SLEEP_H_
#define _WIN_SLEEP_H_

#include <cstdint>
#include <windows.h>

namespace WinSleep {

extern inline void usleep(const uint32_t& microseconds);

}

#endif
