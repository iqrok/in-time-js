#ifndef _TIMESPEC_HELPER_HPP_
#define _TIMESPEC_HELPER_HPP_

#include <cstdint>
#include <time.h>

namespace Timespec {

extern void diff(const struct timespec& a, const struct timespec& b,
	int64_t* ns);

extern void normalize_upper(struct timespec* a);

extern void normalize_lower(struct timespec* a);

extern void copy(struct timespec* dst, const struct timespec& src,
	const int64_t& offset_ns);

extern bool compare(const struct timespec& left, const struct timespec& right);

extern void now(struct timespec* start);

extern uint64_t to_ns(const struct timespec& a);

}

#endif
