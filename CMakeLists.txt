cmake_minimum_required(VERSION 3.15)

cmake_policy(SET CMP0091 NEW)
cmake_policy(SET CMP0042 NEW)
set (CMAKE_CXX_STANDARD 14)

# get version form package.json
execute_process(COMMAND node -p "require('./package.json').version"
		WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
		OUTPUT_VARIABLE PACKAGE_VERSION)
string(REGEX REPLACE "[\r\n\"]" "" PACKAGE_VERSION ${PACKAGE_VERSION})

# incldue node-addon-api dir
execute_process(COMMAND node -p "require('node-addon-api').include"
		WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
		OUTPUT_VARIABLE NODE_ADDON_API_DIR)
string(REGEX REPLACE "[\r\n\"]" "" NODE_ADDON_API_DIR ${NODE_ADDON_API_DIR})

project("InTime" VERSION "${PACKAGE_VERSION}")

set(SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/src")

add_definitions(-DNAPI_VERSION=6 -DNAPI_CPP_EXCEPTIONS)

if(DEFINED ENV{ECAT_BUILD_DEBUG})
	message(STATUS "DEBUG = $ENV{ECAT_BUILD_DEBUG}")
	add_compile_options(-g)
	add_definitions(-DDEBUG=$ENV{ECAT_BUILD_DEBUG}
		-DVERBOSE=$ENV{ECAT_BUILD_DEBUG})
endif()

if (CMAKE_SYSTEM_NAME STREQUAL "Linux")
	add_compile_options(
		-Wall
		-Wfatal-errors
		-O2
		-fexceptions
		-Wpedantic)

	# pthread
	set(THREADS_PREFER_PTHREAD_FLAG ON)
	find_package(Threads REQUIRED)

	# Timespec helper
	set(TIMESPECHELPER_OBJ_NAME "OBJ_TIMESPECHELPER")
	set(SLEEPHELPER_OBJ_LIB "$<TARGET_OBJECTS:${TIMESPECHELPER_OBJ_NAME}>")
	file(GLOB TIMESPECHELPER_SRC_FILES "${SOURCE_DIR}/TimespecHelper.cpp")
	add_library("${TIMESPECHELPER_OBJ_NAME}"
		OBJECT "${TIMESPECHELPER_SRC_FILES}")
	set_target_properties("${TIMESPECHELPER_OBJ_NAME}"
		PROPERTIES POSITION_INDEPENDENT_CODE 1)

elseif(CMAKE_SYSTEM_NAME STREQUAL "Windows")
	add_compile_options(
		-W4
		#~ -wd4710  # suppress failed inlining
		#~ -wd4711  # suppress failed inlining
		-O2
		-EHsc)

	# WinSleep helper
	set(WINSLEEP_OBJ_NAME "OBJ_WINSLEEP")
	set(SLEEPHELPER_OBJ_LIB "$<TARGET_OBJECTS:${WINSLEEP_OBJ_NAME}>")
	file(GLOB WINSLEEP_SRC_FILES "${SOURCE_DIR}/WinSleep.cpp")
	add_library("${WINSLEEP_OBJ_NAME}"
		OBJECT "${WINSLEEP_SRC_FILES}")
	set_target_properties("${WINSLEEP_OBJ_NAME}"
		PROPERTIES POSITION_INDEPENDENT_CODE 1)

else()
	add_compile_options(
		-fexceptions
		-lstdc++
		-O2)
endif()

# TimeoutWorker
set(TIMEOUTWORKER_OBJ_NAME "OBJ_TIMEOUTWORKER")
set(TIMEOUTWORKER_OBJ_LIB "$<TARGET_OBJECTS:${TIMEOUTWORKER_OBJ_NAME}>")
file(GLOB TIMEOUTWORKER_SRC_FILES "${SOURCE_DIR}/TimeoutWorker.cpp")
add_library("${TIMEOUTWORKER_OBJ_NAME}"
	OBJECT "${TIMEOUTWORKER_SRC_FILES}")
target_include_directories("${TIMEOUTWORKER_OBJ_NAME}"
	PRIVATE "${CMAKE_JS_INC}"
		"${NODE_ADDON_API_DIR}"
		"${NAPI_INCLUDE_DIR}")
set_target_properties("${TIMEOUTWORKER_OBJ_NAME}"
	PROPERTIES POSITION_INDEPENDENT_CODE 1)

# DelayWorker
set(DELAYWORKER_OBJ_NAME "OBJ_DELAYWORKER")
set(DELAYWORKER_OBJ_LIB "$<TARGET_OBJECTS:${DELAYWORKER_OBJ_NAME}>")
file(GLOB DELAYWORKER_SRC_FILES "${SOURCE_DIR}/DelayWorker.cpp")
add_library("${DELAYWORKER_OBJ_NAME}"
	OBJECT "${DELAYWORKER_SRC_FILES}")
target_include_directories("${DELAYWORKER_OBJ_NAME}"
	PRIVATE "${CMAKE_JS_INC}"
		"${NODE_ADDON_API_DIR}"
		"${NAPI_INCLUDE_DIR}")
set_target_properties("${DELAYWORKER_OBJ_NAME}"
	PROPERTIES POSITION_INDEPENDENT_CODE 1)

# InTime.node
set(INTIMENODE "${PROJECT_NAME}")
file(GLOB INTIMENODE_SRC_FILES "${SOURCE_DIR}/InTime.cc" "${CMAKE_JS_SRC}")
add_library("${INTIMENODE}"
	SHARED "${INTIMENODE_SRC_FILES}"
		"${TIMEOUTWORKER_OBJ_LIB}"
		"${DELAYWORKER_OBJ_LIB}"
		"${SLEEPHELPER_OBJ_LIB}")
set_target_properties("${INTIMENODE}"
	PROPERTIES POSITION_INDEPENDENT_CODE 1
	PREFIX ""
	SUFFIX ".node")
target_include_directories("${INTIMENODE}"
	PRIVATE "${CMAKE_JS_INC}"
		"${NODE_ADDON_API_DIR}"
		"${NAPI_INCLUDE_DIR}")
target_link_libraries("${INTIMENODE}" PRIVATE "${CMAKE_JS_LIB}")
if (CMAKE_SYSTEM_NAME STREQUAL "Linux")
	target_link_libraries("${INTIMENODE}" PRIVATE Threads::Threads)
endif()
