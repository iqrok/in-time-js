const {
		SetInterval,
		SetTimeout,
		ClearInterval,
		CancelTimeout,
		DelayMs,
		DelayUs,
	} = require('..');

const duration = 15_000;
const interval = 10;
const length = Math.ceil(duration / interval);
const interval_us = interval * 1e3;
const busy_us = 350;

let idx1 = 0;
let idx2 = 0;
let idx3 = 0;

console.time('SetInterval1');
const interval1 = SetInterval((elapsed) => {
	if(++idx1 >= length){
		console.timeEnd('SetInterval1');
		console.time('SetInterval1');

		idx1 = 0;
	}
}, interval_us - 50, busy_us);

console.time('SetInterval2');
const interval2 = SetInterval((elapsed) => {
	if(++idx2 >= length){
		console.timeEnd('SetInterval2');
		console.time('SetInterval2');

		idx2 = 0;
	}
}, interval_us);

console.time('JS_setInterval');
const interval3 = setInterval((elapsed) => {
	if(++idx3 >= length){
		console.timeEnd('JS_setInterval');
		console.time('JS_setInterval');

		idx3 = 0;
	}
}, interval);

console.time('done');
const timeouts = [
	SetTimeout((elapsed) => {
		console.timeEnd('done');
		ClearInterval(interval1);
		ClearInterval(interval2);
		clearInterval(interval3);

		console.log('DONE', elapsed);
	}, 90_000_000),

	SetTimeout((elapsed) => {
		console.log("okay", Date.now(), elapsed);
	}, 1_000_000, busy_us),

	SetTimeout(() => {
		console.log("this will be cancelled!");
	}, 3_000_000),
];

CancelTimeout(timeouts[2]);

(async () => {
	console.time("DelayMs");
	await DelayMs(500, 1);
	console.timeEnd("DelayMs");

	console.time("DelayUs");
	await DelayUs(500_000, 200);
	console.timeEnd("DelayUs");
})();
